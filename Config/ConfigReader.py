import configparser
import xlrd as xl
from CompareData.Core.BBMDataIntegrity.CompareBBMdata import testdb

__author__ = 'Sujay.Bose'

""" Reading the config file to fetch the variables """
config = configparser.ConfigParser()
config.read('Config.ini')
fileName = config.get('fileloc', 'SOURCEFILE')
sheetName = config.get('fileloc', 'SHEETNAME')
resultFile = config.get('fileloc', 'RESULTFILE')
effDate = config.get('dates', 'EFFDATE')
endDate = config.get('dates', 'ENDDATE')

""" Reading the Excel sheet from the location """
Book = xl.open_workbook(fileName)
sheet = Book.sheet_by_index(0)
for row in range(1, sheet.nrows):
    sQuery = sheet.cell_value(row, 1)
    tQuery = sheet.cell_value(row, 2)
    key = sheet.cell_value(row, 3)
    sConnection = sheet.cell_value(row, 4)
    tConnection = sheet.cell_value(row, 5)
    sDB = sheet.cell_value(row, 6)
    tDB = sheet.cell_value(row, 7)
    resultFileName = sheet.cell_value(row, 8)
    sColumns = sheet.cell_value(row, 9)
    columns = sColumns.split(',')
    testdb(resultFile, sConnection, tConnection, sQuery, tQuery, resultFileName, columns, key)













