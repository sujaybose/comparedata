import os
import time
import warnings
import pyodbc
from CompareData.Core.Utilities.utilities import compare

__author__ = 'Sujay.Bose'


def testdb(resultFile, sConnection, tConnection, sQuery, tQuery, resultFileName, columns, key):
    warnings.filterwarnings("ignore")
    beginTime = time.time()

    # Specify the path
    matchFile = resultFile + resultFileName + "_match.txt"
    unMatchFile = resultFile + resultFileName + "_unmatch.txt"

    """Check if file exits delete"""
    if os.path.exists(matchFile):
        os.remove(matchFile)
    if os.path.exists(unMatchFile):
        os.remove(unMatchFile)

    try:
        recordsProcessed = 0
        rowLimit = 5000000


        """Setting up the connection"""
        keyList = []
        sourceRowConnection = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server};' + sConnection)
        sourceRowCursor = sourceRowConnection.cursor()
        targetRowConnection = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server};' + tConnection)

        if sourceRowConnection == targetRowConnection:
            targetRowCursor = sourceRowCursor
        else:
            targetRowCursor = targetRowConnection.cursor()

        """Executing the queries"""
        sourceRowCursor.execute(sQuery)
        targetRowCursor.execute(tQuery)

        while True:
            batchTime = time.time()
            sourceRowResult = sourceRowCursor.fetchmany(rowLimit)
            if not sourceRowResult:
                break
            targetRowResult = targetRowCursor.fetchmany(rowLimit)
            if not targetRowResult:
                break

            sourceRowObjectList = []

            for i, sourceRow in enumerate(sourceRowResult):
                sourceKeyString = ''
                sourceRowObject = {}
                for j, sourceRowValue in enumerate(sourceRow):
                    sourceRowObject[columns[j]] = sourceRowValue
                    if columns[j] in keyList:
                        sourceKeyString = sourceKeyString + str(sourceRowValue)
                sourceRowObject["compareKey"] = sourceKeyString
                sourceRowObjectList.append(sourceRowObject)
            print("SourceRowObjects: ")
            print(sourceRowObjectList)

            targetRowObjectList = []
            for m, targetRow in enumerate(targetRowResult):
                targetKeyString = ''
                targetRowObject = {}
                for n, targetRowValue in enumerate(targetRow):
                    targetRowObject[columns[n]] = targetRowValue
                    if columns[n] in keyList:
                        targetKeyString = targetKeyString + str(targetRowValue)
                targetRowObject["compareKey"] = targetKeyString
                targetRowObjectList.append(targetRowObject)
            print("TargetRowObjects: ")
            print(targetRowObjectList)

            compare(sourceRowObjectList, targetRowObjectList, matchFile, unMatchFile, key)
            recordsProcessed = recordsProcessed + len(targetRowObjectList)
            #print(str(recordsProcessed) + ' rows compared')
            print('Batch compare time for Target: ' + str(
                len(targetRowObjectList)) + ' rows and Source: ' + str(
                len(sourceRowObjectList)) + ' rows: ' + str(time.time() - batchTime))

        sourceRowCursor.close()
        targetRowCursor.close()
        print('Total Time: ' + str(time.time() - beginTime))

    except Exception as e:
        print(e)
