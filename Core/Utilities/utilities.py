from operator import itemgetter
import json


def compare(sourceResultList, targetResultList, matchFile, unMatchFile, key):
    MatchFile = open(matchFile, "a", encoding='utf-8')
    UnMatchFile = open(unMatchFile, "a", encoding='utf-8')
    UnMatchFile.write("--------------UnMatched Records for Comparision Key : " + key + "---------------------\n")
    MatchFile.write("----------------Matched Records for Comparision Key : " + key + "---------------------\n")

    sortedSourceResultList = sorted(sourceResultList, key=itemgetter(key))
    sortedTargetResultList = sorted(targetResultList, key=itemgetter(key))
    targetPointer = 0
    sourcePointer = 0
    targetLength = len(sortedTargetResultList)
    sourceLength = len(sortedSourceResultList)
    if sourceLength > 0:
        sourceItem = sortedSourceResultList[sourcePointer]
    if targetLength > 0:
        targetItem = sortedTargetResultList[targetPointer]
    while targetPointer < targetLength and sourcePointer < sourceLength:
        sourceItem = sortedSourceResultList[sourcePointer]
        targetItem = sortedTargetResultList[targetPointer]
        if targetItem is None:
            continue
        else:
            if str.strip(str(sourceItem[key])) == str.strip(str(targetItem[key])):
                targetPointer = targetPointer + 1
                sourcePointer = sourcePointer + 1
                if targetItem == sourceItem:
                    MatchFile.write(str(sourceItem[key]))
                    MatchFile.write('\n')
                else:
                    for item in sourceItem:
                        if sourceItem[item] != targetItem[item]:
                            UnMatchFile.write(
                                str(sourceItem[key]) + ' ' + item + ' ' + json.dumps(
                                    str(sourceItem[item])) + ' ' + json.dumps(
                                    str(targetItem[item])) + '\n')

            elif str.strip(str(sourceItem[key])) < str.strip(str(targetItem[key])):
                sourcePointer = sourcePointer + 1
                UnMatchFile.write(str(sourceItem[key]))
                UnMatchFile.write(' Not Found in Source \n')
            else:
                targetPointer = targetPointer + 1
                # UnMatchFile.write(targetItem[key] + ' Not Found in Source' + '\n')
                UnMatchFile.write(str(targetItem[key]))
                UnMatchFile.write(' Not Found in Destination \n')
    while targetPointer < targetLength:
        targetItem = sortedTargetResultList[targetPointer]
        targetPointer = targetPointer + 1
        # UnMatchFile.write(targetItem[key] + ' Not Found in Source' + '\n')
        UnMatchFile.write(str(targetItem[key]))
        UnMatchFile.write(' Not Found in Source  \n')
    while sourcePointer < sourceLength:
        sourceItem = sortedSourceResultList[sourcePointer]
        sourcePointer = sourcePointer + 1
        # UnMatchFile.write(sourceItem[key] + ' Not Found in Target' + '\n')
        UnMatchFile.write(str(sourceItem[key]))
        UnMatchFile.write(' Not Found in Destination  \n')
    MatchFile.close()
    UnMatchFile.close()
